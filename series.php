<div class="album text-muted">
    <aside class="col-sm-3 ml-sm-auto blog-sidebar">
        <div class="sidebar-module sidebar-module-inset">
            <h4>Ordenar</h4>
            <ol class="list-unstyled">
                <li><a href="?orden=alfabetico">Ordenar A-Z</a></li>
                <li><a href="?orden=numerico">Ordenar por cantidad de numeros</a></li>
            </ol>
        </div>
    </aside>
        <div class="container">
            <div class="row">
                <?php
                if(isset($_REQUEST["orden"])){
                    $orden=$_REQUEST["orden"];
                }else{
                    $orden="";
                }

                $series = $conexion->getSeries($orden);


                foreach($series as $serie){
                    //Obtenemos la cantidad de números de cada serie
                    //$serie->setNumeros($conexion->getCountNumerosBySerie($serie->getId()));
                    ?>
                    <div class="card">
                        <a href="?tipo_pagina=numeros&id_serie=<?=$serie->getId()?>">
                            <img class="index-portada" src="img/numeros/<?=$serie->getPortada()?>" title="<?=$serie->getDescripcion()?>">
                        </a>
                        <p><?=$serie->getNombre();?> (<?=$serie->getNumeros();?>)</p>
                    </div>

                    <?php
                }
                ?>
            </div>
        </div>


</div>