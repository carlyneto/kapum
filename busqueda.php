<?php

$busqueda=$_POST["busqueda"];

//var_dump($busqueda);exit();
//Buscamos en numeros, autores y series
$series=$conexion -> getSeries("busqueda",$busqueda);
$numeros=$conexion->getNumeros("busqueda",$busqueda);
?>

<div class="container">
    <h2>Series</h2>
            <div class="row">
                <?php
                foreach($series as $serie){
                    //Obtenemos la cantidad de números de cada serie
                    //$serie->setNumeros($conexion->getCountNumerosBySerie($serie->getId()));
                    ?>
                    <div class="card">
                        <a href="?tipo_pagina=numeros&id_serie=<?=$serie->getId()?>">
                            <img class="index-portada" src="img/numeros/<?=$serie->getPortada()?>" title="<?=$serie->getDescripcion()?>">
                        </a>
                        <p><?=$serie->getNombre();?> (<?=$serie->getNumeros();?>)</p>
                    </div>

                    <?php
                }
                ?>
</div>
</div>

    <div class="container">
        <h2>Numeros</h2>
        <div class="row">
            <?php
            //var_dump($numeros);
            foreach($numeros as $numero){
                ?>
                <div class="card">
                    <a href="?tipo_pagina=numero&id_numero=<?=$numero->getId()?>">
                        <img class="index-portada" src="img/numeros/<?=$numero->getPortada()?>" title="<?=$numero->getDescripcion()?>">
                    </a>
                    <p ><?=$numero->getNombreSerie();?> #<?=$numero->getNumero();?></p>
                </div>

                <?php
            }
            ?>
        </div>
    </div>