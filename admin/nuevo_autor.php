<?php

if(isset($_REQUEST["id_autor"])){
    $id_autor=$_REQUEST["id_autor"];
    $modo="update";
}else{
    $modo="insert";
}
?>

<h1>Nuevo autor</h1>
<form method="post" action="?tipo_pagina=guardar_autor&modo=<?=$modo?>" enctype="multipart/form-data">
    <div class="form-group row">
        <label class="col-form-label col-sm-1">Nombre</label>
        <input name="nombre" class="form-control col-sm-3" type="text"/>

        <label class="col-form-label col-sm-1">Apellidos</label>
        <input name="apellidos" class="form-control col-sm-3" type="text"/>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-1">Pais</label>
        <input name="pais" class="form-control col-sm-3" type="text"/>
        <label class="col-form-label col-sm-1">Foto</label>
        <input class="form-control col-sm-3" type="file" name="foto"/>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-1">Biografia</label>
        <textarea class="form-control col-sm-7" type="number" name="biografia"></textarea>
    </div>

    <div class="form-group row ">
        <button type="submit" class="btn btn-primary col-sm-3 btn-anadir-numero">Añadir autor</button>
    </div>
</form>

<?php


?>
