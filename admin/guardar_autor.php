<?php
//Tenemos que VALIDAR los datos antes de introducir nada en la bbdd: AJAX
$autor = new Autor();
$autor->setAtributesByForm($_POST);

if(isset($_FILES["foto"])){
    $autor->saveFileFoto($_FILES["foto"]);
}
$conexion->insertAutor($autor);

header("Location: ?tipo_pagina=gestion_autores");