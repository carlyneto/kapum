<h1>Numeros</h1>


<?php
if(isset($_REQUEST["id_serie"])){
    //Si tenemos un id en el enlace venimos de ver los numeros de una serie en concreto
    $id_serie=$_REQUEST["id_serie"];

    $numeros = $conexion->getNumerosBySerie($id_serie);
    $nombre_serie=$conexion->getSerieById($id_serie)->getNombre();
    $titulo_pagina="Numeros de ".$nombre_serie;

}else{
    $id_serie=0;
    $numeros = $conexion->getNumeros();
    $titulo_pagina="Todos los números";
}


?>
<h2><?=$titulo_pagina?></h2>
<?php if($id_serie!=0){
?>
    <a href="?tipo_pagina=nuevo_numero&id_serie=<?=$id_serie?>" role="button" class="btn btn-success">Nuevo numero</a>

    <?php
}
?>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Numero</th>
            <th>Portada</th>
            <th>Descripcion</th>
            <th>Fecha publicacion</th>
            <th>Precio</th>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php

        //var_dump($numeros);exit();
        foreach($numeros as $numero){
            ?>
            <tr>
                <td><?= $numero->getNumero(); ?>-<a href="?tipo_pagina=gestion_numeros&id_serie=<?=$numero->getIdSerie()?>"><?= $numero->getNombreSerie(); ?></a></td>
                <td><img src="../img/numeros/<?= $numero->getPortada(); ?>" class="portada-dashboard"></td>
                <td><?= $numero->getDescripcion(); ?></td>
                <td><?= $numero->getFechaPublicacion(); ?></td>
                <td><?= $numero->getPrecio() ?></td>
                <td><a href="?tipo_pagina=borrar_numero&id_numero=<?= $numero->getId() ?>" role="button" class="btn btn-danger" id="btn_borrar_<?= $numero->getId() ?>">Borrar</a></td>
                <td><a href="?tipo_pagina=nuevo_numero&id_numero=<?= $numero->getId() ?>" role="button" class="btn btn-warning" id="btn_editar_<?= $numero->getId() ?>">Editar</a></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>