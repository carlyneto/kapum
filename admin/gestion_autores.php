<h2>Autores</h2>
<a href="?tipo_pagina=nuevo_autor" role="button" class="btn btn-success">Nuevo autor</a></td>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Apellidos</th>
            <th>Biografia</th>
            <th>Pais</th>
            <th>Foto</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php
        $autores = $conexion->getAutores();
        //var_dump($autors);exit();
        foreach($autores as $autor){
            ?>
            <tr>
                <td><?= $autor->getNombre() ?></td>
                <td><?= $autor->getApellidos() ?></td>
                <td><?= $autor->getBiografia() ?></td>
                <td><?= $autor->getPais() ?></td>
                <td><img src="../img/autores/<?= $autor->getFoto() ?>" class="portada-dashboard"></td>
                <td><a href="?tipo_pagina=borrar_autor&id_autor=<?= $autor->getId() ?>" role="button" class="btn btn-danger" id="btn_borrar_<?= $autor->getId() ?>">Borrar</a></td>
                <td><a href="?tipo_pagina=nuevo_autor&id_autor=<?= $autor->getId() ?>" role="button" class="btn btn-warning" id="btn_editar_<?= $autor->getId() ?>">Editar</a></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>