
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Panel de control</title>

    <!-- Bootstrap core CSS -->
    <link href="../css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="../css/dashboard.css" rel="stylesheet">

    <!-- Estilos propios -->
    <link href="../css/propios.css" rel="stylesheet">
</head>

<body>
<?php
session_start();
if(!$_SESSION["login"]){
    header ("Location: ../index.php");
}
include "../class/database.php";
include "../class/serie.php";
include "../class/numero.php";
include "../class/autor.php";
include "../config/config.php";

$conexion=new Db();

if(isset($_REQUEST["tipo_pagina"])){
    $tipo_pagina=$_REQUEST["tipo_pagina"];
}else{
    $tipo_pagina="";
}

?>
<header>
    <nav class="navbar navbar-expand-md navbar-dark fixed-top bg-dark">
        <a class="navbar-brand" href="#">Dashboard</a>
        <button class="navbar-toggler d-lg-none" type="button" data-toggle="collapse" data-target="#navbarsExampleDefault" aria-controls="navbarsExampleDefault" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExampleDefault">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item active">
                    <a class="nav-link" href="../index.php">Indice</span></a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="#">Settings</a>
                </li>
            </ul>
            <!--
            <form class="form-inline mt-2 mt-md-0">
                <input class="form-control mr-sm-2" type="text" placeholder="Search" aria-label="Search">
                <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Search</button>
            </form>
            -->
        </div>
    </nav>
</header>

<div class="container-fluid">
    <div class="row">
        <nav class="col-sm-3 col-md-2 d-none d-sm-block bg-light sidebar">
            <ul class="nav nav-pills flex-column">
                <li class="nav-item">
                    <!--<a class="nav-link active" href="index.php">Series <span class="sr-only">(current)</span></a>-->
                    <a class="nav-link" href="index.php">Series</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?tipo_pagina=gestion_numeros">Numeros</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="?tipo_pagina=gestion_autores">Autores</a>
                </li>

            </ul>

        </nav>

        <main role="main" class="col-sm-9 ml-sm-auto col-md-10 pt-3">
            <?php
            switch($tipo_pagina){
                case "nueva_serie":
                    include "nueva_serie.php";
                    break;

                case "editar_serie":
                    include "editar_serie.php";
                    break;
                case "anadir_serie":
                    include "anadir_serie.php";
                    break;
                case "borrar_serie":
                    include "borrar_serie.php";
                    break;
               case "guardar_numero":
                    include "guardar_numero.php";
                    break;
                case "borrar_numero":
                    include "borrar_numero.php";
                    break;
                case "borrar_autor":
                    include "borrar_autor.php";
                    break;
                case "nuevo_numero":
                    include "nuevo_numero.php";
                    break;
                case "gestion_numeros";
                    include "gestion_numeros.php";
                    break;
                case "gestion_autores";
                    include "gestion_autores.php";
                    break;
                case "nuevo_autor":
                    include "nuevo_autor.php";
                    break;
                case "guardar_autor":
                    include "guardar_autor.php";
                    break;

                default:
                    include "gestion_series.php";
                    break;
            }

            ?>
        </main>
    </div>
</div>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script>
<?php
    $conexion->desconectar();

?>
</body>
</html>
