<h1>Vista general</h1>

<h2>Series</h2>
<a href="?tipo_pagina=nueva_serie" role="button" class="btn btn-success">Nueva serie</a></td>

<div class="table-responsive">
    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nombre</th>
            <th>Descripción</th>
            <th>Genero</th>
            <th>Estado</th>
            <th>Perioidicidad</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php
        $series = $conexion->getSeries();
        //var_dump($series);exit();
        foreach($series as $serie){
            ?>
            <tr>
                <td><?= $serie->getNombre() ?></td>
                <td><?= $serie->getDescripcion() ?></td>
                <td><?= $serie->getGenero() ?></td>
                <td><?= $serie->getEstado() ?></td>
                <td><?= $serie->getPerioidicidad() ?></td>
                <td><a href="?tipo_pagina=borrar_serie&id_serie=<?= $serie->getId() ?>" role="button" class="btn btn-danger" id="btn_borrar_<?= $serie->getId() ?>">Borrar</a></td>
                <td><a href="?tipo_pagina=nueva_serie&id_serie=<?= $serie->getId() ?>" role="button" class="btn btn-warning" id="btn_editar_<?= $serie->getId() ?>">Editar</a></td>
                <td><a href="?tipo_pagina=gestion_numeros&id_serie=<?= $serie->getId() ?>" role="button" class="btn btn-info" id="btn_numeros_<?= $serie->getId() ?>">Numeros</a></td>
            </tr>
            <?php
        }
        ?>

        </tbody>
    </table>
</div>