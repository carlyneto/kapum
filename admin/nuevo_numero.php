<?php
/*
if(isset($_REQUEST["modo"])){
    $modo = $_REQUEST["modo"];
}*/


//Si le hemos pasado un id_serie queremos añadir nuevo numero
if(isset($_REQUEST["id_serie"])){
    $id_serie = $_REQUEST["id_serie"];
    $nombre_serie = $conexion->getSerieById($id_serie)->getNombre();
    $modo = "datos_nuevo_numero";
}else{
    $id_serie=0;
}


//Si le hemos pasado un id_numero, queremos actualizar un nuevo numero
if(isset($_REQUEST["id_numero"])){
    $id_numero = $_REQUEST["id_numero"];
    $modo = "datos_antiguo_numero";

}else{
    $id_numero=0;
}




switch($modo){

    case "datos_antiguo_numero":

        $numero=$conexion->getNumeroById($id_numero);
        $numero_form = $numero->getNumero();
        $nombre_serie=$numero->getNombreSerie();
        $id_serie=$numero->getIdSerie();

        //$existe_serie=true;
        $modo = "update";

        break;

    case "datos_nuevo_numero":
        //Buscamos el último número de la serie para añadirlo directamente al formulario
        $ultimo_numero = $conexion->getLastNumeroByIdSerie($id_serie);
        $numero_form=$ultimo_numero+1;

        $modo="insert";
        break;
    case "exit":
        header("Location: index.php");
        break;

}

?>
<h1>Nuevo número - <?=$nombre_serie?></h1>
<form method="post" action="?tipo_pagina=guardar_numero&modo=<?=$modo?>" enctype="multipart/form-data">
    <div class="form-group row">
        <label class="col-form-label col-sm-2">Descripción</label>
        <textarea name="descripcion" class="form-control col-sm-3" name="descripcion" rows="3"
                  placeholder="Descripción del número"><?php if($modo=="update") echo $numero->getDescripcion();?></textarea>

        <label class="col-form-label col-sm-1">Portada</label>
        <input name="portada" class="form-control col-sm-4" type="file"/>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2">Numero</label>
        <input name="numero" class="form-control col-sm-3" type="number" min="0" value="<?=$numero_form?>"/>
    </div>

    <div class="form-group row">
        <label class="col-form-label col-sm-2">Fecha publicación</label>
        <input class="form-control col-sm-3" value="<?php if($modo=="update") echo $numero->getFechaPublicacion();?>" type="date" name="fecha_publicacion"/>
    </div class="form-group row">

    <div class="form-group row">
        <label class="col-form-label col-sm-2">Precio</label>
        <input class="form-control col-sm-3" value="<?php if($modo=="update") echo $numero->getPrecio();?>" type="number" min="0" step="0.1" name="precio"/>

        <div class="col-sm-3">
            <button type="submit" class="btn btn-primary">Añadir numero</button>
        </div>
    </div>
    <?php if($id_numero){ ?> <input type="hidden" value="<?=$id_numero?>" name="id"/> <?php } ?>
    <?php if($id_serie) { ?> <input type="hidden" value="<?=$id_serie?>" name="id_serie"/> <?php } ?>
    <?php if($nombre_serie) { ?> <input type="hidden" value="<?=$nombre_serie?>" name="nombre_serie"/> <?php } ?>


</form>
