<h1>Nueva serie</h1>

<?php

if(isset($_POST["nombre"])&&
    isset($_POST["perioidicidad"])&&
    isset($_POST["genero"])&&
    isset($_POST["descripcion"])){
    //Se ha enviado el formulario
    //insertamos serie


    $serie= new Serie();
    $serie->setAtributesByForm($_POST);
    $serie->insertSerie();
    header ("Location: ?tipo_pagina=nuevo_numero&id_serie=".$serie->getId());


}

$existe_serie=false;
$tipo_pagina="nueva_serie";
if(isset($_REQUEST["id_serie"])){
    //Queremos editar una serie
    $id_serie=$_REQUEST["id_serie"];
    $serie=$conexion->getSerieById($id_serie);
    $existe_serie=true;
    $tipo_pagina="editar_serie";
}



?>
<form method="post" action="?tipo_pagina=<?=$tipo_pagina?>">
    <input type="hidden" name="id" value="<?php if($existe_serie)echo $serie->getId();?>"/>
    <div class="form-group">
        <input value="<?php if($existe_serie)echo $serie->getNombre();?>" name="nombre" type="text" class="form-control" placeholder="Nombre de la serie">
    </div>
    <div class="form-group">
        <textarea name="descripcion" class="form-control" name="descripcion" rows="3" placeholder="Descripción de la serie"><?php if($existe_serie)echo trim($serie->getDescripcion());?></textarea>
    </div>

    <div class="row">
            <label class="col-md-2">Perioidicidad</label>
            <select class="form-control col-md-2" name="perioidicidad">
                <option>Semanal</option>
                <option>Quincenal</option>
                <option>Mensual</option>
                <option>Trimestral</option>
                <option>Anual</option>
            </select>

        <div class="col-md-2" >
            <input value="<?php if($existe_serie)echo $serie->getGenero();?>" name="genero" type="text" class="form-control" placeholder="Genero">
        </div>

        <div class="col-md-2">
            <label class="form-check-label">
                <input <?php if($existe_serie&&$serie->getEstado()=="Finalizada")echo 'checked';?> name="estado" type="checkbox" class="form-check-input">
                Serie finalizada
            </label>
        </div>


        <div class="col-sm-3">
            <button type="submit" class="btn btn-primary">Añadir serie</button>
        </div>
    </div>
</form>


