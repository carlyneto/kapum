<div class="album text-muted">
    <!--
    <aside class="col-sm-3 ml-sm-auto blog-sidebar">
        <div class="sidebar-module sidebar-module-inset">
            <h4>Ordenar</h4>
            <ol class="list-unstyled">
                <li><a href="?orden=alfabetico">Ordenar A-Z</a></li>
                <li><a href="?orden=numerico">Ordenar por cantidad de numeros</a></li>
            </ol>
        </div>
    </aside>-->
        <div class="container">
            <div class="row">
                <?php
                if(isset($_REQUEST["orden"])){
                    $orden=$_REQUEST["orden"];
                }else{
                    $orden="";
                }

                $autores = $conexion->getAutores();


                foreach($autores as $autor){
                    //Obtenemos la cantidad de números de cada serie
                    //$autor->setNumeros($conexion->getCountNumerosBySerie($autor->getId()));
                    ?>
                    <div class="card">
                        <a href="?tipo_pagina=autor&id_autor=<?=$autor->getId()?>">
                            <img class="index-portada" src="img/autores/<?=$autor->getFoto()?>" title="<?=$autor->getBiografia()?>">
                        </a>
                        <p><?=$autor->getNombre();?> <?=$autor->getApellidos();?></p>
                    </div>

                    <?php
                }
                ?>
            </div>
        </div>


</div>