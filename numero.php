<?php
//Recogemos el id_numero
if(isset($_REQUEST["id_numero"])){
    $id_numero=$_REQUEST["id_numero"];
}else{
    $id_numero=0;
}

$numero=$conexion->getNumeroById($id_numero);



?>


<div class="album text-muted">

    <div class="container">
        <div class="row">

            <?php
            //var_dump($numeros);
                ?>
                <div class="card card-numero">
                    <h2><?=$numero->getNombreSerie()?> - <?=$numero->getNumero()?></h2>
                    <img class="portada-numero" src="img/numeros/<?=$numero->getPortada()?>" title="<?=$numero->getDescripcion()?>">
                    <p class="card-text"><b>Fecha de publicación</b> <?=$numero->getFechaPublicacion();?></p>
                    <p class="card-text"><b>Precio</b> <?=$numero->getPrecio();?> Euros</p>
                    <p class="card-text"><b>Descripcion</b> <?=$numero->getDescripcion();?></p>
                </div>
        </div>
    </div>
</div>
