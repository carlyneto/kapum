<div class="album text-muted">
    <div class="container">
        <form action="check_login.php" method="post" class="form-login" id="form-login">
            <h2 class="form-signin-heading">Inicia sesión</h2>
            <label class="sr-only">Usuario</label>
            <input name="usuario" type="text" class="form-control" placeholder="Usuario" required autofocus>
            <label class="sr-only">Password</label>
            <input name="contrasena" type="password"  class="form-control" placeholder="Contraseña" required>
            <button class="btn btn-lg btn-primary btn-block" type="submit">Login</button>
            <!--<a class="btn btn-primary" href="#" role="button">Crear cuenta</a>-->
        </form>
        <?php
        if(isset($_REQUEST["mensaje"])){
            ?>
            <div class="alert alert-danger" role="alert"><?php echo "Contraseña o usuario incorrectos";?></div>
            <?php
        }?>
    </div>
</div>