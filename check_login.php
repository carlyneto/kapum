<?php
session_start();
include "config/config.php";
include "class/database.php";
$user=$_POST["usuario"];
$pass=$_POST["contrasena"];
$conexion=new Db();
$res=$conexion->checkLogin($user,$pass);
if($res){
    //Login correcto
    $_SESSION["login"]=true;
    header("Location: admin");

}else{
    $_SESSION["login"]=false;
    header("Location: index.php?tipo_pagina=login&mensaje=error");
    //Login incorrecto
}