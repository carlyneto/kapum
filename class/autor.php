<?php

class Autor
{

    public $_data = null;

    function __construct()
    {
        $this->_data['id'] = 0;
        $this->_data['id_serie'] = 0;
        $this->_data['nombre'] = null;
        $this->_data['apellidos'] = null;
        $this->_data['biografia'] = null;
        $this->_data['pais'] = null;
        $this->_data['foto'] = null;
    }

    /*
     * Esta funcion recoge los datos directamente desde la BBDD
     * $datos es una array asociativo con los nombres de los campos de la bbdd.series
     * */
    public function setAtributes($datos) {
        $this->setId($datos["id"]);
        $this->setNombre($datos["nombre"]);
        $this->setApellidos($datos["apellidos"]);
        $this->setBiografia($datos["biografia"]);
        $this->setPais($datos["pais"]);
        $this->setFoto($datos["foto"]);

    }

    /*
     * Esta funcion recoge los datos directamente desde la BBDD
     * $datos es una array asociativo con los nombres de los campos de la bbdd.series
     * */
    public function setAtributesByForm($datos) {
        $this->setNombre($datos["nombre"]);
        $this->setApellidos($datos["apellidos"]);
        $this->setBiografia($datos["biografia"]);
        $this->setPais($datos["pais"]);

    }

    public function setFotoName(){
        return $this->getNombre()."_".$this->getApellidos()."_".substr($this->getBiografia(),0,10).".jpg";
    }

    public function saveFileFoto($fichero){
        $this->setFoto($this->setFotoName());
        //var_dump($fichero);
        $directorio = "../img/autores/";
        // Establece la ruta final del fichero (manteniendo su nombre original)
        $ruta_final = $directorio . $this->getFoto();
        // Ruta donde se encuentra temporalmente el fichero pendiente de la ubicación final
        $fichero_temporal = $fichero["tmp_name"];

        // Mueve el fichero de su ubicación temporal a la ruta definitiva en el servidor
        move_uploaded_file($fichero_temporal, $ruta_final);

    }

    //----------------------GETTERS---------------------------
    public function get($dato)
    {
        return $this->_data[$dato];
    }

    public function getId()
    {
        return $this->get("id");
    }

    public function getNombre()
    {
        return $this->get("nombre");
    }

    public function getApellidos()
    {
        return $this->get("apellidos");
    }

    public function getBiografia()
    {
        return $this->get("biografia");
    }

    public function getPais()
    {
        return $this->get("pais");
    }

    public function getFoto()
    {
        return $this->get("foto");
    }

    public function getIdSerie()
    {
        return $this->get("id_serie");
    }

    public function getData()
    {
        return $this->_data;
    }

    //-------------------SETTERS---------------------------
    public function setGeneral($clave, $valor)
    {
        $this->_data[$clave] = $valor;
    }

    public function setId($valor)
    {
        $this->setGeneral("id", $valor);
    }

    public function setNombre($valor)
    {
        $this->setGeneral("nombre", $valor);
    }

    public function setApellidos($valor)
    {
        $this->setGeneral("apellidos", $valor);
    }

    public function setBiografia($valor)
    {
        $this->setGeneral("biografia", $valor);
    }

    public function setFoto($valor)
    {
        $this->setGeneral("foto", $valor);
    }

    public function setIdSerie($valor)
    {
        $this->setGeneral("id_serie", $valor);
    }

    public function setPais($valor)
    {
        $this->setGeneral("pais", $valor);
    }

}
