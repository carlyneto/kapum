<?php

class Numero
{

    public $_data = null;

    function __construct()
    {
        $this->_data['id'] = 0;
        $this->_data['id_serie'] = 0;
        $this->_data['numero'] = 0;
        $this->_data['descripcion'] = null;
        $this->_data['portada'] = null;
        $this->_data['fecha_publicacion'] = null;
        $this->_data['precio'] = 0;
        $this->_data['nombre_serie'] = null;
    }

    public function saveFilePortada($fichero){
        //var_dump($fichero);
        $directorio = "../img/numeros/";
        // Establece la ruta final del fichero (manteniendo su nombre original)
        $ruta_final = $directorio . $this->getPortada();
        // Ruta donde se encuentra temporalmente el fichero pendiente de la ubicación final
        $fichero_temporal = $fichero["tmp_name"];

        // Mueve el fichero de su ubicación temporal a la ruta definitiva en el servidor
        move_uploaded_file($fichero_temporal, $ruta_final);

    }

    /*
     * Esta funcion recoge los datos directamente desde la BBDD
     * $datos es una array asociativo con los nombres de los campos de la bbdd.series
     * */
    public function setAtributes($datos) {
        if($datos["id"]){
            $this->setId($datos["id"]);
        }

        $this->setNumero($datos["numero"]);
        $this->setDescripcion($datos["descripcion"]);
        $this->setFechaPublicacionFormateada($datos["fecha_publicacion"]);
        $this->setPrecio($datos["precio"]);

        if($datos["nombre_serie"]){
            $this->setNombreSerie($datos["nombre_serie"]);
        }

        if($datos["id_serie"]!=0){
            $this->setIdSerie($datos["id_serie"]);
        }

        $this->setPortada($this->getNameFilePortada());

    }
    /*
     * Esta funcion recoge los datos directamente desde el formulario de nuevo numero
     * $datos es una array asociativo con los nombres de los names del form
     * */
    public function setAtributesFormNuevo($datos) {
        if($datos["id"]>0){
            $this->setId($datos["id"]);
        }
        $this->setNumero($datos["numero"]);
        $this->setDescripcion($datos["descripcion"]);
        $this->setFechaPublicacionFormateada($datos["fecha_publicacion"]);
        $this->setPrecio($datos["precio"]);
        $this->setIdSerie($datos["id_serie"]);
        $this->setNombreSerie($datos["nombre_serie"]);

        $this->setPortada($this->getNameFilePortada());

    }

    //con el numero y el titulo de la serie hacemos un nombre personalizado para el fichero
    public function getNameFilePortada(){
        $numero=$this->getNumero();
        if($numero<10){
            $numero="0".$numero;
        }


        $nombre=strtolower($this->getNombreSerie());
        $nombre=str_replace(" ","_",$nombre);
        return $nombre."_".$numero.".jpg";
    }


    public function setFechaPublicacionFormateada($fecha){
        /*$fecha_array=explode("-",$fecha);
        var_dump($fecha);
        $fecha_correcta=$fecha_array[2]."-".$fecha_array[1]."-".$fecha_array[0];
        var_dump($fecha_correcta);exit();*/
        $this->setFechaPublicacion($fecha);

    }




    /*
    public static function  getSeries(){
            $conexion= new Db();
            $sql = "SELECT * FROM series";
            $res = $conexion->lanzar_consulta($sql);
            $series = array();
            while ($fila = $res->fetch_assoc()) {
                $serie = new Serie();
                $serie->setAtributes($fila);
                $series[] = $serie;
            }
            $conexion->desconectar();
            //var_dump($series);exit();
            return $series;


    }
*/
    /*
    public function setConsultas($consultas) {
        //Tenemos un array con las consultas
        $arr_consultas = $this->getConsultas();
        foreach ($consultas as $consulta) {
            $arr_consultas[] = new Consulta($consulta);
        }
        $this->setGeneral("consultas", $arr_consultas);
    }

    //------------------------------------------------


    public static function loadConsultasDefault() {

        $filtro_default = new FiltradoConsultas();
        $filtro_default->getConsultasFiltradasDefault();
        $datos_consultas = $filtro_default->getDatos();
        //Ya tenemos un array con todos los objetos consultas, ahora tenemos que enviarlos al html
        //REcogemos los datos de las consultas, no los objetos
        return $datos_consultas;
    }

    public static function loadConsultas($post) {

        $filtro = $_SESSION["filtro"];
        $filtro = new FiltradoConsultas();
        $filtro->setAtributos($post);
        $filtro->getConsultasFiltradas();
        //Ya tenemos un array con todos los objetos consultas, ahora tenemos que enviarlos al html
        //REcogemos los datos de las consultas, no los objetos
        $datos_consultas = $filtro->getDatos();
        return $datos_consultas;
    }

    //Este método recibe un post y crea el objeto filtro


    */


    /*

    /* En funci�n de los criterios del filtro, creamos un array con lo que ser�
     * la parte del WHERE de la consulta.

     *      */
    /*
        public function creaArrayCriterios() {

            $criterios = array();
            if ($this->getPalabras_filtrar()) {
                $porcentajes_palabras = "%" . $this->getPalabras_filtrar() . "%";
    //                    var_dump($porcentajes_palabras);

                $criterios["palabras_filtrar"] = parametros("AND (consultas.consulta LIKE '%s' OR consultas.descripcion LIKE '%s' ", $porcentajes_palabras, $porcentajes_palabras);

                if ($this->getBuscar_comentarios()) {
                    $criterios["palabras_filtrar"] = $criterios["palabras_filtrar"] . " OR comentarios.comentario LIKE '%" . $this->getPalabras_filtrar() . "%'";
                }
                $criterios["palabras_filtrar"] .= ")";
            }

            if ($this->getId_proyecto()) {
                $criterios["id_proyecto"] = "AND id_proyecto=" . $this->getId_proyecto();
            }

            $criterios["estados"] = "AND (";

            if ($this->getAlDia()) {
                $criterios["estados"] .= " (corregir=0 AND borrar=0 AND desuso=0) OR";
            }
            if ($this->getDesuso()) {
                $criterios["estados"] .= " desuso=1 OR";
            }
            if ($this->getBorrar()) {
                $criterios["estados"] .= " borrar=1 OR";
            }
            if ($this->getCorregir()) {
                $criterios["estados"] .= " corregir=1 OR";
            }
            $criterios["estados"] .= ")";
            //Con esto quitamos los posibles (...OR)
            $criterios["estados"]= str_replace("OR)", ")",$criterios["estados"] );



            if($criterios["estados"] == "AND ()"){
                $criterios["estados"]=null;
            }


    //        var_dump($criterios);
            return $criterios;
        }

        public function getWhereFiltrado() {
            $where = "";
            $criterios = $this->getCriterios(); //Array con criterios de búsqueda

            foreach ($criterios as $criterio) {
    //            switch ($criterio) {
    //                case "borrar=1";
    //                    $where .= $criterio . " OR ";
    //                    break;
    //                case "corregir=1";
    //                    $where .= $criterio . " OR ";
    //                    break;
    //                case "desuso=1";
    //                    $where .= $criterio . " OR ";
    //                    break;
    //                case "corregir=0 AND borrar=0 AND desuso=0";
    //                    $where .= "(" . $criterio . ") OR ";
    //                    break;
    //                default;
    //                    $where .="(". $criterio . ") AND ";
    //                    break;
    //            }
                $where.=$criterio." ";
            }
            //borrar=1, id_proyecto=4...
            $where = trim($where, 'AND ');
            $where = trim($where, 'OR ');



            return $where;
        }

        public function getConsultasFiltradas() {

            $sql = $this->getSQL();
            $_SESSION["sql"] = $sql;
            $res = query($sql);
            $consultas = Utilidades::res2Array($res);
            $this->setConsultas($consultas);
            return $consultas;
        }

        public function getSQL() {
            $sql = "";
            if ($this->getBuscar_comentarios()) {
                //Buscamos también en comentarios
                $sql = "SELECT * FROM consultas INNER JOIN comentarios ON consultas.id_consulta=comentarios.id_consulta";
            } else {
                //No buscamos en comentarios
                $sql = "SELECT * FROM consultas";
            }

            $sql = $sql . " WHERE id_proyecto IN (SELECT id_proyecto FROM proyectos WHERE eliminado=0)";



            $where = $this->getWhereFiltrado();
            if ($where) {
                $sql = $sql . " AND " . $where . " GROUP BY consultas.id_consulta";
            }

            $sql = $sql . " ORDER BY corregir DESC,desuso ASC,borrar ASC,id_proyecto ASC ";

            return $sql;
        }

        public function getConsultasFiltradasDefault() {

            $sql = "SELECT * FROM consultas WHERE id_proyecto=(SELECT id_proyecto FROM proyectos WHERE eliminado=0 ORDER BY 1 ASC LIMIT 1)"
                . "ORDER BY corregir DESC,desuso ASC,borrar ASC";
            $res = query($sql);
            $consultas = Utilidades::res2Array($res);
            $this->setConsultas($consultas);
            return $consultas;
        }

        //Devuelve los datos de todas las consultas de este filtro.
        public function getDatos() {
            $datos = array();
            $consultas = $this->getConsultas();
            foreach ($consultas as $consulta) {
                $datos[] = $consulta->getData();
            }
            return $datos;
        }
    */
    //----------------------GETTERS---------------------------
    public function get($dato)
    {
        return $this->_data[$dato];
    }

    public function getId()
    {
        return $this->get("id");
    }

    public function getNombreSerie()
    {
        return $this->get("nombre_serie");
    }

    public function getNumero()
    {
        return $this->get("numero");
    }

    public function getDescripcion()
    {
        return $this->get("descripcion");
    }

    public function getPortada()
    {
        return $this->get("portada");
    }

    public function getFechaPublicacion()
    {
        return $this->get("fecha_publicacion");
    }

    public function getPrecio()
    {
        return $this->get("precio");
    }

    public function getIdSerie()
    {
        return $this->get("id_serie");
    }

    public function getData()
    {
        return $this->_data;
    }

    //-------------------SETTERS---------------------------
    public function setGeneral($clave, $valor)
    {
        $this->_data[$clave] = $valor;
    }

    public function setId($valor)
    {
        $this->setGeneral("id", $valor);
    }

    public function setNumero($valor)
    {
        $this->setGeneral("numero", $valor);
    }

    public function setDescripcion($valor)
    {
        $this->setGeneral("descripcion", $valor);
    }

    public function setPortada($valor)
    {
        $this->setGeneral("portada", $valor);
    }

    public function setFechaPublicacion($valor)
    {
        $this->setGeneral("fecha_publicacion", $valor);
    }

    public function setPrecio($valor)
    {
        $this->setGeneral("precio", $valor);
    }

    public function setNombreSerie($valor)
    {
        $this->setGeneral("nombre_serie", $valor);
    }
    public function setIdSerie($valor)
    {
        $this->setGeneral("id_serie", $valor);
    }

}
