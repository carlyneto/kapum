<?php

class Db
{

    private $conexion;


    function __construct()
    {
        $this->conectar();
    }

    function conectar()
    {
        $this->conexion = new mysqli(DB_HOST, DB_USER,
            DB_PASSWORD, DB_NAME);
    }

    function desconectar()
    {
        $this->conexion->close();
    }

    function ultimo_id()
    {
        return $this->conexion->insert_id;
    }

    function lanzar_consulta($sql, $parametros = array()){

        $sentencia = $this->conexion->prepare($sql);
        if (!empty($parametros)) {
            $tipos = "";
            foreach ($parametros as $parametro) {
                if (gettype($parametro) == "string")
                    $tipos = $tipos . "s";
                else
                    $tipos = $tipos . "i";
            }
            $sentencia->bind_param($tipos, ...$parametros);
        }

        $sentencia->execute();
        $resultado = $sentencia->get_result();
        $sentencia->close();

        return $resultado;
    }


    //Funciones de recogida de datos
    //Devuelve un array de resultados con:
    function getSeries($tipo = "",$cadena=""){
        switch($tipo){
            case "alfabetico":
                $sql = "SELECT series.id,series.nombre, series.descripcion,series.estado,series.perioidicidad,series.genero,count(id_serie) as total from series inner join numeros on series.id=numeros.id_serie group by id_serie ORDER BY nombre";
                break;
            case "numerico":
                $sql = "SELECT series.id,series.nombre, series.descripcion,series.estado,series.perioidicidad,series.genero,count(id_serie) as total from series inner join numeros on series.id=numeros.id_serie group by id_serie order by total desc";
                break;
            case "busqueda":
                $sql = "SELECT series.id,series.nombre,series.descripcion,series.estado,series.perioidicidad,series.genero,count(id_serie) as total 
from series inner join numeros on series.id=numeros.id_serie 
where series.nombre LIKE ? OR series.descripcion LIKE ? group by id_serie";

                break;
            default:
                $sql = "SELECT series.id,series.nombre, series.descripcion,series.estado,series.perioidicidad,series.genero,count(id_serie) as total from series inner join numeros on series.id=numeros.id_serie group by id_serie";
                break;
        }

        if($cadena!=""){

            $param1="%".$cadena."%";
            $param2="%".$cadena."%";

            $params = array($param1,$param2);
            $res = $this->lanzar_consulta($sql,$params);


        }else{
            $res = $this->lanzar_consulta($sql);
        }

        $series = array();
        while ($fila = $res->fetch_assoc()) {
            $serie = new Serie();
            $serie->setAtributes($fila);
            $serie->setNumeros($fila["total"]);
            $serie->setPortada($this->getPortadaById($serie->getId()));

            $series[] = $serie;
        }

        //var_dump($series);exit();
        return $series;
    }

    function getSerieById($id){
        $sql = "SELECT * FROM series WHERE id=?";
        $param=array($id);
        $res = $this->lanzar_consulta($sql,$param);
        while ($fila = $res->fetch_assoc()) {
            $serie = new Serie();
            $serie->setAtributes($fila);
            $serie->setPortada($this->getPortadaById($serie->getId()));
        }

        return $serie;
    }

    //Recogemos la pportada del primer numero de esta serie
    function getPortadaById($id){
        $sql = "select numero,portada from numeros where id_serie=? order by numero asc limit 1";
        $param=array($id);
        $res = $this->lanzar_consulta($sql,$param);
        $portada="";
        while ($fila = $res->fetch_assoc()) {
            $portada= $fila["portada"];
        }

        return $portada;

    }

    function deleteSerieById($id){
        $sql = "DELETE FROM series WHERE id=?";
        $param = array($id);
        $res = $this->lanzar_consulta($sql,$param);
    }

    public function updateSerie($serie){
        $sql="UPDATE series SET nombre=?,descripcion=?,estado=?,perioidicidad=?,genero=? WHERE id=?";
        $params = array($serie->getNombre(),$serie->getDescripcion(),$serie->getEstado(),$serie->getPerioidicidad(),$serie->getGenero(),$serie->getId());
        $this->lanzar_consulta($sql,$params);

    }

    public function getLastNumeroByIdSerie($id){
        $sql = "SELECT numero FROM numeros WHERE id_serie=? ORDER BY 1 DESC LIMIT 1";
        $param=array($id);
        $res = $this->lanzar_consulta($sql,$param);
        $numero=0;

        while ($fila = $res->fetch_assoc()) {
            $numero=$fila["numero"];
        }

        return $numero;
    }



    //------------------- BUSQUEDa -------------------

    public function busquedaTotal($cadena){

        /*$series=$this->$busquedaSeries($cadena);*/



    }

    /*
    public function busquedaSeries($cadena){
        $sql = "SELECT series.id,series.nombre,series.descripcion,series.estado,series.perioidicidad,series.genero,count(id_serie) as total 
from series inner join numeros on series.id=numeros.id_serie 
where series.nombre LIKE ? OR series.descripcion LIKE ? group by id_serie";

        $param1="%".$cadena."%";
        $param2="%".$cadena."%";

        $params = array($param1,$param2);

        $res = $this->lanzar_consulta($sql,$params);
        //var_dump($res->fetch_assoc());

        $series = array();
        while ($fila = $res->fetch_assoc()) {
            //var_dump($fila);
            $serie = new Serie();
            $serie->setAtributes($fila);
            $serie->setNumeros($fila["total"]);
            $serie->setPortada($this->getPortadaById($serie->getId()));

            $series[] = $serie;
        }

        //var_dump($series);exit();
        return $series;
    }
*/

    //Devuelve los numeros de una serie
    function getNumerosBySerie($id_serie){
        $sql = "SELECT nu.id, nu.numero, nu.portada,nu.fecha_publicacion, nu.precio, nu.descripcion,nu.id_serie, se.nombre as nombre_serie 
FROM numeros nu INNER JOIN series se ON nu.id_serie=se.id WHERE id_serie=?";
        $param = array($id_serie);
        //var_dump($sql);
        $res = $this->lanzar_consulta($sql,$param);

        $numeros = array();
        while ($fila = $res->fetch_assoc()) {
            $numero = new Numero();
            $numero->setAtributes($fila);
            $numeros[] = $numero;
        }

        //var_dump($numeros);exit();
        return $numeros;
    }
    /*
    //Devuelve la cantidad de numeros de una serie
    function getCountNumerosBySerie($id_serie){
        $sql = "SELECT COUNT(*) as total FROM numeros WHERE id_serie=?";
        $param = array($id_serie);
        //var_dump($sql);
        $res = $this->lanzar_consulta($sql,$param);
        $total_numeros=0;
        while ($fila = $res->fetch_assoc()) {
            $total_numeros=$fila["total"];
        }

        //var_dump($numeros);exit();
        return $total_numeros;
    }*/

    //Devuelve TODOS los numeros
    function getNumeros($busqueda="",$cadena=""){
        if($busqueda!=""&&$cadena!=""){
            $sql = "SELECT nu.id, nu.numero, nu.portada,nu.fecha_publicacion,nu.descripcion, nu.precio,nu.id_serie, se.nombre as nombre_serie 
FROM numeros nu INNER JOIN series se ON nu.id_serie=se.id WHERE nu.descripcion LIKE ? ORDER BY nu.id_serie, nu.numero";
            $param1="%".$cadena."%";
            $params = array($param1);
            $res = $this->lanzar_consulta($sql,$params);

        }else{
            $sql = "SELECT nu.id, nu.numero, nu.portada,nu.fecha_publicacion,nu.descripcion, nu.precio,nu.id_serie, se.nombre as nombre_serie 
FROM numeros nu INNER JOIN series se ON nu.id_serie=se.id ORDER BY nu.id_serie, nu.numero";
            $res = $this->lanzar_consulta($sql);
        }


        $numeros = array();
        while ($fila = $res->fetch_assoc()) {
            $numero = new Numero();
            $numero->setAtributes($fila);
            $numeros[] = $numero;
        }

        //var_dump($numeros);exit();
        return $numeros;
    }

    //Recibe un numero y lo inserta en la BBDD
    function insertNumero($numero){
        $sql="INSERT INTO numeros (id_serie,numero,descripcion,portada,fecha_publicacion,precio) VALUES (?,?,?,?,?,?)";
        $params = array($numero->getIdSerie(),$numero->getNumero(),$numero->getDescripcion(),$numero->getPortada(),$numero->getFechaPublicacion(),$numero->getPrecio());
        $this->lanzar_consulta($sql,$params);

    }

    //Recibe un numero y lo inserta en la BBDD
    function updateNumero($numero){
        $sql = "UPDATE numeros SET numero=?,descripcion=?,portada=?,fecha_publicacion=?,precio=? WHERE id=?";
        $params = array($numero->getNumero(),$numero->getDescripcion(),$numero->getPortada(),$numero->getFechaPublicacion(),$numero->getPrecio(),$numero->getId());
        $this->lanzar_consulta($sql,$params);

    }

    //Borra un numero por idnumero
    function deleteNumeroById($id){
        $sql = "DELETE FROM numeros WHERE id=?";
        $param = array($id);
        $res = $this->lanzar_consulta($sql,$param);

    }

/*
    //Recoge numero por id
    function getNumeroById($id){
        $sql = "SELECT * FROM numeros WHERE id=?";
        $param=array($id);
        $res = $this->lanzar_consulta($sql,$param);
        while ($fila = $res->fetch_assoc()) {
            $numero = new Numero();
            $numero->setAtributes($fila);
        }

        return $numero;
    }*/

    //Devuelve TODOS los numeros
    function getNumeroById($id){
        $sql = "SELECT nu.id, nu.numero, nu.portada,nu.fecha_publicacion,nu.descripcion, nu.precio,nu.id_serie, se.nombre as nombre_serie 
FROM numeros nu INNER JOIN series se ON nu.id_serie=se.id WHERE nu.id=?";
        $param=array($id);
        $res = $this->lanzar_consulta($sql,$param);
        while ($fila = $res->fetch_assoc()) {
            $numero = new Numero();
            $numero->setAtributes($fila);
        }

        return $numero;
    }

    //-------------AUTORES-------------
    //Recoge todos los autores

    function getAutores(){
        $sql = "SELECT * FROM autores";
        $res = $this->lanzar_consulta($sql);
        $autores = array();
        while ($fila = $res->fetch_assoc()) {
            $autor = new Autor();
            $autor->setAtributes($fila);

            $autores[] = $autor;
        }

        //var_dump($series);exit();
        return $autores;
    }

    function getAutorById($id){
        $sql = "SELECT * FROM autores WHERE id=?";
        $params= array($id);
        $res = $this->lanzar_consulta($sql,$params);
        $autor = new Autor();
        while ($fila = $res->fetch_assoc()) {
            $autor->setAtributes($fila);
        }

        //var_dump($series);exit();
        return $autor;
    }

    function deleteAutorById($id){
        $sql = "DELETE FROM autores WHERE id=?";
        $param = array($id);
        $this->lanzar_consulta($sql,$param);
    }

    function insertAutor($autor){
        $sql = "INSERT INTO autores (nombre,apellidos,biografia,pais,foto) VALUES (?,?,?,?,?)";
        $param = array($autor->getNombre(),$autor->getApellidos(),$autor->getBiografia(),$autor->getPais(),$autor->getFoto());
        $this->lanzar_consulta($sql,$param);
    }




    //--------------------- USUARIOS ---------------------
    function checkLogin($user,$pass){
        $sql = "SELECT * FROM usuarios WHERE usuario=? AND contrasena=?";
        $params = array($user,sha1($pass));
        $res=$this->lanzar_consulta($sql,$params);

        if($res->num_rows > 0){
            return true;
        }else{
            return false;
        }
    }



}