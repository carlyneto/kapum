<?php
//Recogemos el id_numero
if(isset($_REQUEST["id_autor"])){
    $id_autor=$_REQUEST["id_autor"];
}else{
    $id_autor=0;
}

$autor=$conexion->getAutorById($id_autor);



?>


<div class="album text-muted">

    <div class="container container-autor">
        <h2><?=$autor->getNombre()?> <?=$autor->getApellidos()?></h2>
        <div class="row">
            <?php
            //var_dump($autors);
            ?>
            <div class="card-autor">
                <img class="foto-autor" src="img/autores/<?=$autor->getFoto()?>" title="<?=$autor->getBiografia()?>">
                <p><b>Pais: </b> <?=$autor->getPais();?></p>
                <p><b>Biografia: </b> <?=$autor->getBiografia();?></p>
            </div>
        </div>
    </div>
</div>
