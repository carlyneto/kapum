
<!doctype html>
<html lang="es">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../../../favicon.ico">

    <title>Kapum</title>

    <?php
        include "inc/styles.php";
    ?>

</head>

<body>
<?php
include "class/database.php";
include "config/config.php";
include "class/serie.php";
include "class/numero.php";
include "class/autor.php";

$conexion = new Db();
?>

<nav class="navbar navbar-expand-lg navbar-light bg-dark">
    <a class="navbar-brand link" href="index.php">Kapum</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
        <ul class="navbar-nav">
            <li class="nav-item">
                <a class="nav-link link" href="?tipo_pagina=numeros">Numeros</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link" href="?tipo_pagina=series">Series</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link" href="?tipo_pagina=autores">Autores</a>
            </li>
            <li class="nav-item">
                <a class="nav-link link" href="?tipo_pagina=login">Login</a>
            </li>
        </ul>
    </div>
    <form class="form-inline" action="?tipo_pagina=busqueda" method="post">
        <input class="form-control mr-sm-2" type="search" name="busqueda" placeholder="Buscar" aria-label="Buscar">
        <button class="btn btn-outline-success my-2 my-sm-0" type="submit">Buscar</button>
    </form>
</nav>

<main role="main">

    <section class="jumbotron text-center">
        <div class="container">
            <h1 class="jumbotron-heading">Kapum</h1>
            <p class="lead text-muted">Localiza tus series favoritas</p>
        </div>
    </section>
<!--
    <div class="lateral">
        <ul class="nav flex-column">
            <li class="nav-item">
                <a class="nav-link active" href="#">Ordenar A-Z</a>
            </li>
            <li class="nav-item">
                <a class="nav-link" href="#">Ordenar por cantidad de números</a>
            </li>
        </ul>
    </div>
-->

    <?php


    if(isset($_REQUEST["tipo_pagina"])){
        $tipo_pagina=$_REQUEST["tipo_pagina"];

    }else{
        $tipo_pagina="series";
    }

    switch($tipo_pagina){
        case "series":
            include "series.php";
            break;

        case "numeros":
            include ("numeros.php");
            break;

        case "numero":
            include "numero.php";
            break;

        case "autores":
            include "autores.php";
            break;

        case "autor":
            include "autor.php";
            break;

        case "login":
            include "login.php";
            break;
        case "busqueda":
            include "busqueda.php";
            break;

    }
    ?>

</main>

<footer class="footer">
    <div class="container">
        <p class="text-muted">Web creada por Carlos Nieto</p>
    </div>
</footer>

<!-- Bootstrap core JavaScript
================================================== -->
<!-- Placed at the end of the document so the pages load faster -->
<script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
<script>window.jQuery || document.write('<script src="../../../../assets/js/vendor/jquery.min.js"><\/script>')</script>
<script src="../../../../assets/js/vendor/popper.min.js"></script>
<script src="../../../../dist/js/bootstrap.min.js"></script>
<script src="../../../../assets/js/vendor/holder.min.js"></script>
<script>
    Holder.addTheme('thumb', {
        bg: '#55595c',
        fg: '#eceeef',
        text: 'Thumbnail'
    });
</script>

<?php
$conexion->desconectar();
?>
</body>
</html>
