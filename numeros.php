<?php
//Recogemos el id_serie
if(isset($_REQUEST["id_serie"])){
    $id_serie=$_REQUEST["id_serie"];
}else{
    $id_serie=0;
}

if($id_serie==0){
    //Queremos mostrar todos los numeros
    $modo="todas_series";
}else{
    //Solo queremos mostrar los numeros de una serie
    $modo="una_serie";
}

switch($modo){
    case "todas_series":
        $numeros=$conexion->getNumeros();
        $titulo_pagina="Todos los numeros";
        //var_dump($numeros);
        break;
    case "una_serie":
        $numeros=$conexion->getNumerosBySerie($id_serie);
        $nombre_serie=$numeros[0]->getNombreSerie();
        $titulo_pagina="Numeros de ".$nombre_serie;
        break;

}

?>


<div class="album text-muted">

    <div class="container">
        <h2><?=$titulo_pagina?></h2>
        <div class="row">
            <?php
            //var_dump($numeros);
            foreach($numeros as $numero){

                ?>
                <div class="card">
                    <a href="?tipo_pagina=numero&id_numero=<?=$numero->getId()?>">
                        <img class="index-portada" src="img/numeros/<?=$numero->getPortada()?>" title="<?=$numero->getDescripcion()?>">
                    </a>
                    <p ><?=$numero->getNombreSerie();?> #<?=$numero->getNumero();?></p>
                </div>

                <?php
            }
            ?>
        </div>
    </div>
</div>
