-- phpMyAdmin SQL Dump
-- version 4.7.0
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 15-12-2017 a las 20:30:33
-- Versión del servidor: 10.1.25-MariaDB
-- Versión de PHP: 5.6.31

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `kapum`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores`
--

CREATE TABLE `autores` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `apellidos` varchar(100) NOT NULL,
  `biografia` varchar(500) NOT NULL,
  `pais` varchar(100) NOT NULL,
  `foto` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `autores`
--

INSERT INTO `autores` (`id`, `nombre`, `apellidos`, `biografia`, `pais`, `foto`) VALUES
(7, 'Frank', 'Miller', 'Famosisimo, guionista y dibujante', 'EEUU', 'Frank_Miller_Famosisimo.jpg'),
(8, 'Geoff', 'Johns', 'Estructurador del DC actual.', 'EEUU', 'Geoff_Johns_Estructura.jpg'),
(9, 'David', 'RubÃ­n', 'dibujante espaÃ±ol que ha llegado al mercado americano', 'EspaÃ±a', 'David_Rubin_dibujante .jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `autores-series`
--

CREATE TABLE `autores-series` (
  `id_autor` int(11) NOT NULL,
  `id_serie` int(11) NOT NULL,
  `puesto` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `numeros`
--

CREATE TABLE `numeros` (
  `id` int(11) NOT NULL,
  `id_serie` int(11) NOT NULL,
  `numero` int(11) NOT NULL,
  `descripcion` varchar(500) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `portada` varchar(200) NOT NULL,
  `fecha_publicacion` date NOT NULL,
  `precio` decimal(5,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `numeros`
--

INSERT INTO `numeros` (`id`, `id_serie`, `numero`, `descripcion`, `portada`, `fecha_publicacion`, `precio`) VALUES
(1, 1, 1, 'La historia del origen empieza aqui', 'batman_01.jpg', '2017-12-05', '3.00'),
(2, 1, 2, 'El segundo arco de todo', 'batman_02.jpg', '2017-12-05', '2.00'),
(3, 2, 1, 'El origen de los vengadores', 'vengadores_01.jpg', '2017-12-08', '5.00'),
(4, 2, 2, 'Continua con la historia de Kang', 'vengadores_02.jpg', '2017-12-08', '4.00'),
(5, 4, 1, 'El primer numero de superman', 'superman_01.jpg', '2017-12-13', '5.00'),
(9, 10, 1, 'La historia empieza aquÃ­', 'regerso_al_futuro_01.jpg', '2015-01-01', '4.00'),
(10, 10, 2, 'la historia continua', 'regerso_al_futuro_02.jpg', '2015-02-02', '5.00'),
(11, 1, 3, 'Seguimos con la historia de batman', 'batman_03.jpg', '2016-01-01', '8.00'),
(12, 10, 3, 'Tercer numero de la coleccion', 'regerso_al_futuro_03.jpg', '2017-05-25', '4.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `series`
--

CREATE TABLE `series` (
  `id` int(11) NOT NULL,
  `nombre` varchar(200) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `descripcion` varchar(1000) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `estado` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `perioidicidad` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL,
  `genero` varchar(100) CHARACTER SET utf8 COLLATE utf8_spanish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `series`
--

INSERT INTO `series` (`id`, `nombre`, `descripcion`, `estado`, `perioidicidad`, `genero`) VALUES
(1, 'Batman', 'Las aventuras del murcielago', 'Abierta', 'Mensual', 'Superheroes'),
(2, 'Vengadores', 'Los heroes mas poderosos de la tierra', 'Finalizada', 'Semanal', 'Accion'),
(4, 'Superman', 'Las aventuras del Kryptoniano', 'Abierta', 'Mensual', 'Superheroes'),
(10, 'Regerso al futuro', 'Lo que no te contaron las peliculas', 'Finalizada', 'Mensual', 'Ciencia ficcion'),
(11, 'Lobezno', 'La serie de lobezno', 'Abierta', 'Mensual', 'Accion');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE `usuarios` (
  `id` int(11) NOT NULL,
  `nombre` varchar(100) NOT NULL,
  `usuario` varchar(100) NOT NULL,
  `contrasena` varchar(100) NOT NULL,
  `rol` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `nombre`, `usuario`, `contrasena`, `rol`) VALUES
(1, 'admin', 'admin', 'd033e22ae348aeb5660fc2140aec35850c4da997', 'administrador');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `autores`
--
ALTER TABLE `autores`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `numeros`
--
ALTER TABLE `numeros`
  ADD PRIMARY KEY (`id`),
  ADD KEY `id_serie` (`id_serie`);

--
-- Indices de la tabla `series`
--
ALTER TABLE `series`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `autores`
--
ALTER TABLE `autores`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT de la tabla `numeros`
--
ALTER TABLE `numeros`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT de la tabla `series`
--
ALTER TABLE `series`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `numeros`
--
ALTER TABLE `numeros`
  ADD CONSTRAINT `numeros_ibfk_1` FOREIGN KEY (`id_serie`) REFERENCES `series` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
